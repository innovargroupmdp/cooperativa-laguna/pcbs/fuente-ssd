EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1350 2350 2050 3300
U 6031687A
F0 "power" 50
F1 "power.sch" 50
F2 "IN-AC" I R 3400 2500 50 
F3 "GND" I R 3400 3000 50 
F4 "VCC" I R 3400 3500 50 
F5 "V0" I R 3400 4000 50 
$EndSheet
$Comp
L power_supply-rescue:Screw_Terminal_01x02-Connector J1
U 1 1 606D827B
P 4200 2700
F 0 "J1" H 4280 2692 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 4280 2601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4200 2700 50  0001 C CNN
F 3 "~" H 4200 2700 50  0001 C CNN
	1    4200 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2500 3400 2500
Wire Wire Line
	3700 3000 3400 3000
Wire Wire Line
	3850 2500 3850 2700
Wire Wire Line
	3850 2700 4000 2700
Wire Wire Line
	4000 2800 3700 2800
Wire Wire Line
	3700 2800 3700 3000
$EndSCHEMATC
